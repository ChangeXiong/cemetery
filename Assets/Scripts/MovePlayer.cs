using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePlayer : MonoBehaviour
{
    private readonly static int HurtTriggerHash = Animator.StringToHash("HurtTrigger");
    private readonly static int IsPlayerRunHash = Animator.StringToHash("IsPlayerRun");
    private readonly static int IsPlayerJumpHash = Animator.StringToHash("IsPlayerFloat");

    private Rigidbody2D rbbody;
    public float moveSpeed;
    private SpriteRenderer spriteRenderer;
    private Animator anim;

    [SerializeField] private float jumpVelocity;
    [SerializeField] private Vector3 footOffset;
    [SerializeField] private float footRadius;
    [SerializeField] private LayerMask groundLayerMask;

    private bool isGround;
    private bool canDoubleJump;
    private void Awake()
    {
        rbbody = GetComponent<Rigidbody2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        anim = GetComponent<Animator>();
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
/*        if (Input.GetKeyDown(KeyCode.Space))
        {
            anim.SetTrigger(HurtTriggerHash);
        }
        if (anim.GetCurrentAnimatorStateInfo(0).IsName("Hero_Hurt"))
        {
            rbbody.velocity = Vector2.zero;
            return;
        }*/
        float moveHorizontal = Input.GetAxis("Horizontal") * moveSpeed;
        rbbody.velocity = new Vector2(moveHorizontal, rbbody.velocity.y);

        if(moveHorizontal != 0)
        {
            spriteRenderer.flipX = moveHorizontal < 0;
        }

        anim.SetBool(IsPlayerRunHash, Mathf.Abs(moveHorizontal) > 0 && isGround);
        anim.SetBool(IsPlayerJumpHash, !isGround);
        PlayerJump();
    }
    private void FixedUpdate()
    {
       Collider2D hitCollider = Physics2D.OverlapCircle(transform.position + footOffset, footRadius,groundLayerMask);
        isGround = hitCollider != null;
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = isGround ? Color.green : Color.yellow;
        Gizmos.color = canDoubleJump && !isGround ? Color.blue : Gizmos.color;
        Gizmos.DrawWireSphere(transform.position + footOffset, footRadius);
    }
    private void PlayerJump()
    {
        if (isGround)
        {
            canDoubleJump = true;
        }
        if (Input.GetButtonDown("Jump")){

            if (isGround)
            {
                rbbody.velocity = new Vector2(rbbody.velocity.x, jumpVelocity);
            }
            else if (canDoubleJump)
            {
                rbbody.velocity = new Vector2(rbbody.velocity.x, jumpVelocity);
                canDoubleJump = false;
            }

            
        }
    }
}
